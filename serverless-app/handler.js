const serverless = require("serverless-http");
const express = require("express");
const atlassianfile = require("./atlassian-connect.json");

const bodyParser = require("body-parser");
const compression = require("compression");
const cookieParser = require("cookie-parser");
const errorHandler = require("errorhandler");
const morgan = require("morgan");

const http = require("http");
const path = require("path");
const os = require("os");
const helmet = require("helmet");
const nocache = require("nocache");
// atlassian-connect-express also provides a middleware
const ace = require("atlassian-connect-express");

const hbs = require("express-hbs");

const app = express();
const addon = ace(app);

const routes = require("./routes");
const { addServerSideRendering } = require("./addServerSideRendering");

// Log requests, using an appropriate formatter by env
const devEnv = app.get("env") === "development";
app.use(morgan(devEnv ? "dev" : "combined"));

// See config.json
const port = addon.config.port();
// const port = 3000
app.set("port", port);

// Configure Handlebars
const viewsDir = path.join(__dirname, "views");
const handlebarsEngine = hbs.express4({ partialsDir: viewsDir });
app.engine("hbs", handlebarsEngine);
app.set("view engine", "hbs");
app.set("views", viewsDir);

// Configure jsx (jsx files should go in views/ and export the root component as the default export)
addServerSideRendering(app, handlebarsEngine);

// Atlassian security policy requirements
// http://go.atlassian.com/security-requirements-for-cloud-apps
// HSTS must be enabled with a minimum age of at least one year
app.use(
  helmet.hsts({
    maxAge: 31536000,
    includeSubDomains: false,
  })
);
app.use(
  helmet.referrerPolicy({
    policy: ["origin"],
  })
);

// Include request parsers
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

// Gzip responses when appropriate
app.use(compression());

// Include atlassian-connect-express middleware
app.use(addon.middleware());

const staticDir = path.join(__dirname, "");
app.use(express.static(staticDir));

// Atlassian security policy requirements
// http://go.atlassian.com/security-requirements-for-cloud-apps
app.use(nocache());

// Show nice errors in dev mode
app.use(errorHandler());

routes(app, addon);

http.createServer(app).listen(port, () => {
  console.log("App server running at http://" + os.hostname() + ":" + port);
  // Enables auto registration/de-registration of app into a host in dev mode
  // if (devEnv) addon.register();
});

// module.exports.handler = serverless(app);
